#pragma once

#include <iostream>
#include <glm/glm.hpp>

namespace minimol {

// class to translate mouse and keyboard interactions to camera information
class ViewInteractor {

public:
    ViewInteractor();

    void updateModifiers(bool shift, bool alt, bool ctrl);
    void updateButtons(bool left, bool right, bool middle);
    void updateKeys(bool space);

    void updatePosition(int pos_x, int pos_y);
    void updateWheel(bool dir_up);

    void updateLeftButton(bool left);
    void updateRightButton(bool right);
    void updateMiddleButton(bool middle);

    void setViewportSize(int width, int height);

    glm::vec3 eye, focus, up_dir;
    float zoom_speed;

    glm::vec3 front_dir() const {
        return glm::normalize(focus - eye);
    }
    glm::vec3 right_dir() const {
        return glm::normalize(glm::cross(up_dir, front_dir()));
    }

private:
    // flags that define the current state
    bool shift, alt, ctrl;
    bool left, right, middle;
    bool space;
    bool wheel_dir_up, wheel_active;
    bool position_valid;
    int pos_x, pos_y;
    int delta_x, delta_y;
    int old_pos_x, old_pos_y;
    int width, height;
    float sensitivity;

    void updateTransformation();
    void rotateSpherical(glm::vec3& target, const glm::vec3& center, glm::vec3& up, bool swap_y = false);
    void rotateUpDir(glm::vec3 &target, const glm::vec3 &axis);
    void moveImagePlane(glm::vec3& target, glm::vec3& center, const glm::vec3& up);
    void moveTowardsCenter(glm::vec3& target, glm::vec3& center, float step);
};

}
