#include "ImageWriter.h"

#ifdef HAS_LIBPNG

#include <png.h>
#include <stdexcept>
#include <stdint.h>
#include <algorithm>

minimol::ImageWriter::ImageWriter(void) {
}

minimol::ImageWriter::~ImageWriter(void) {
}

void minimol::ImageWriter::save(const char* fn, unsigned int width, unsigned int height, DataType type, void* data) {
    FILE *f = fopen(fn, "wb");
    if (f == nullptr) throw std::runtime_error("Unable to create file");

    try {
        png_structp png_ptr = ::png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
        if (!png_ptr) throw std::runtime_error("Unable to create png write structure");

        png_infop info_ptr = ::png_create_info_struct(png_ptr);
        if (!info_ptr) {
            ::png_destroy_write_struct(&png_ptr, NULL);
            throw std::runtime_error("Unable to create png info structure");
        }

        ::png_init_io(png_ptr, f);

        if (::setjmp(png_jmpbuf(png_ptr))) {
            ::png_destroy_write_struct(&png_ptr, &info_ptr);
            throw std::runtime_error("setjmp failed");
        }

        ::png_set_compression_level(png_ptr, 9);
        unsigned int bpp;
        switch (type) {
        case DataType::RGBu8:
            ::png_set_IHDR(png_ptr, info_ptr, width, height, 8, PNG_COLOR_TYPE_RGB,
                    PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
            bpp = 3;
            break;
        case DataType::RGBAu8:
            ::png_set_IHDR(png_ptr, info_ptr, width, height, 8, PNG_COLOR_TYPE_RGB_ALPHA,
                    PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
            bpp = 4;
            break;
        case DataType::Df:
            ::png_set_IHDR(png_ptr, info_ptr, width, height, 16, PNG_COLOR_TYPE_GRAY,
                    PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
            {
                float *datain = static_cast<float*>(data);
                uint16_t *dataout = static_cast<uint16_t*>(data);
                for (unsigned int i = 0; i < width * height; i++, datain++, dataout++) {
                    float v = *datain;
                    if ((v < 0.0) || (v > 1.0)) {
                        v = std::min<float>(1.0, std::max<float>(0.0, v));
                    }
                    *dataout = static_cast<uint16_t>(v * static_cast<float>(UINT16_MAX));
                    uint8_t *b = (uint8_t*)dataout;
                    std::swap(b[0], b[1]);
                }
            }
            bpp = 2;
            break;
        default:
            throw std::logic_error("Unsupported image type");
        }

        png_byte **rows = new png_byte*[height];
        unsigned int stride = width * bpp;
        png_byte *imgData = static_cast<png_byte*>(data);

        for (unsigned int y = 0; y < height; y++) {
            rows[height - (1 + y)] = imgData;
            imgData += stride;
        }

        ::png_set_rows(png_ptr, info_ptr, rows);
        ::png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

        delete[] rows;

        ::png_destroy_write_struct(&png_ptr, &info_ptr);

        fclose(f);

    } catch(...) {
        fclose(f);
        throw;
    }
}

#endif
