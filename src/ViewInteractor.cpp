#include "ViewInteractor.h"

using namespace minimol;

#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifndef M_PI
#define M_PI 3.14159265345
#endif

ViewInteractor::ViewInteractor() : zoom_speed(1.0f) {
    shift = alt = ctrl = false;
    left = right = middle = false;
    wheel_active = false;
    wheel_dir_up = false;
    position_valid = false;
    pos_x = pos_y = 0;
    width = height = 0;
    space = false;
    sensitivity = 0.002f;
}

void ViewInteractor::updateModifiers(bool shift, bool alt, bool ctrl) {
    this->shift = shift;
    this->alt = alt;
    this->ctrl = ctrl;
}

void ViewInteractor::updateButtons(bool left, bool right, bool middle) {
    if (this->left == left && this->right == right && this->middle == middle)
        return;

    this->left = left;
    this->right = right;
    this->middle = middle;

    position_valid = false;
}

void ViewInteractor::updateKeys(bool space) {
    this->space = space;
}

void ViewInteractor::updateLeftButton(bool left) {
    if (this->left == left)
        return;

    this->left = left;
    position_valid = false;
}

void ViewInteractor::updateRightButton(bool right) {
    if (this->right == right)
        return;

    this->right = right;
    position_valid = false;
}

void ViewInteractor::updateMiddleButton(bool middle) {
    if (this->middle == middle)
        return;

    this->middle = middle;
    position_valid = false;
}

void ViewInteractor::updatePosition(int pos_x, int pos_y) {
    if (position_valid) {
        old_pos_x = this->pos_x;
        old_pos_y = this->pos_y;
    } else {
        old_pos_x = pos_x;
        old_pos_y = pos_y;
        position_valid = true;
    }
    this->pos_x = pos_x;
    this->pos_y = pos_y;

    delta_x = pos_x - old_pos_x;
    delta_y = pos_y - old_pos_y;

    updateTransformation();
}

void ViewInteractor::updateWheel(bool dir_up)  {
    wheel_active = true;
    wheel_dir_up = dir_up;

    updateTransformation();

    wheel_active = false;
}

void ViewInteractor::setViewportSize(int width, int height) {
    this->width = width;
    this->height = (height > 0) ? height : 1;
}

void ViewInteractor::updateTransformation() {
    glm::vec3 x, y, z;

    // if the wheel was triggered then move towards the center
    if (wheel_active) {
        if (wheel_dir_up)
            moveTowardsCenter(eye, focus, 0.5);
        else
            moveTowardsCenter(eye, focus, -0.5);
    }

    // check for a rotation action
    if (left) {
        // if no modifiers are active then this is a spherical rotation
        if (!shift && !alt && !ctrl)
            rotateSpherical(eye, focus, up_dir);

        // if shift is pressed then this is a roll
        if (shift && !alt && !ctrl)
            rotateUpDir(up_dir, eye - focus);

        // if ctrl is pressed then move the focus point rather than
        // the eye point
        if (ctrl && !shift && !alt)
            rotateSpherical(focus, eye, up_dir, true);
    }

    // check for a move action which is triggered by the right mouse button
    if (right) {
        // if no modifiers are active then this is just a motion across the
        // the plane orthogonal to the view direction
        if (!shift && !alt && !ctrl)
            moveImagePlane(eye, focus, up_dir);
    }

    // if the middle button was pressed then perform a zoom towards the focus
    if (middle) {
        float dist = -(float)delta_x + delta_y;
        moveTowardsCenter(eye, focus, zoom_speed * dist);
    }
}

void ViewInteractor::moveImagePlane(glm::vec3 &target, glm::vec3 &center, const glm::vec3 &up) {
    // create an orthogonal system where Z is the view direction
    // and x and y two orthogonal directions that define the
    // two rotation planes
    glm::vec3 z = glm::normalize(center - target);
    glm::vec3 x = glm::normalize(glm::cross(up, z));
    glm::vec3 y = glm::cross(z, x);

    float factor = glm::length(center - target);

    float move_x = (float)delta_x / (float)width*factor;
    float move_y = (float)delta_y / (float)height*factor;

    target = target + move_x*x + move_y*y;
    center = center + move_x*x + move_y*y;
}

// perform a spherical rotation
void ViewInteractor::rotateSpherical(glm::vec3 &target, const glm::vec3 &center, glm::vec3 &up, bool swap_y) {
    if (delta_x == 0 && delta_y == 0)
        return;

    float delta_phi = (float)(delta_x*M_PI*sensitivity);
    float delta_theta = (float)(delta_y*M_PI*sensitivity);

    if (swap_y)
        delta_theta *= -1.0;

    // create an orthogonal system where Z is the view direction
    // and x and y two orthogonal directions that define the
    // two rotation planes
    glm::vec3 z = glm::normalize(center - target);
    glm::vec3 x = glm::normalize(glm::cross(up, z));
    glm::vec3 y = glm::cross(z, x);

    glm::vec3 axis = delta_theta*x - delta_phi*y;
    float angle = glm::length(axis);
    axis = glm::normalize(axis);
    target = glm::rotate(target - center, angle, axis) + center;
    up = glm::rotate(up_dir, angle, axis);
}

void ViewInteractor::rotateUpDir(glm::vec3 &target, const glm::vec3 &axis) {
    // calculate the difference angle of the actual position
    // with the center of the window as rotation center and
    // the old position
    float relative_x = static_cast<float>(pos_x - width ) * 0.5f;
    float relative_y = static_cast<float>(pos_y - height) * 0.5f;
    float angle = atan2(relative_y, relative_x) - atan2(relative_y - delta_y, relative_x - delta_x);

    target = glm::rotate(target, angle, glm::normalize(axis));
}

void ViewInteractor::moveTowardsCenter(glm::vec3& target, glm::vec3& center, float step) {
    glm::vec3 dir = center - target;

    if (glm::length(dir) < 2 * step) return;

    target = target + step*glm::normalize(dir);
}
