#pragma once

#include <glm/glm.hpp>
#include <string>
#ifdef HAS_ANTTWEAKBAR
#include <AntTweakBar.h>
#endif
#include "Data.h"
#include <cstdint>
#include "CmdLineParser.h"
#include "ViewInteractor.h"
#include "lib/SplineCalculation.h"
#ifdef __linux
#include <eigen3/Eigen/Dense>
#else
#include <Eigen/Dense>
#endif

namespace minimol {

    class Renderer;

class Window {
public:
    Window(void);
    ~Window(void);

    void SetData(Data *dat);
    void SetRenderer(Renderer* rnd);

    void SetFromParser(CmdLineParser& parser);
    bool timelapse;
    unsigned int timelapsei;
    uint32_t filetime;
    std::string filename;

	//New Spline
	unsigned int degree;
	unsigned int frames;
	unsigned int countControllpoints;

private:
    static void glutDisplay();
    static void glutReshape(int w, int h);
    static void glutMouse(int btn, int state, int x, int y);
    static void glutMouseMotion(int x, int y);
    static void glutKeyboard(unsigned char key, int x, int y);
    static void glutKeyboardUp(unsigned char key, int x, int y);
    static void glutSpecialKeyboard(int keyCode, int x, int y);
    static void glutSpecialKeyboardUp(int keyCode, int x, int y);
    static void glutClose();

    void display();
    void resetCamera();
    void storeCamera();
    void restoreCamera();
#ifdef HAS_LIBPNG
    void saveScreenShot();
#endif
    void saveState();
    void loadState();
    void dumpMatrices();
    void calcClipPlanes(float& d_min, float& d_max);
    void updateProjMat();
    void saveData();

	//New Spline
	void renderSpline();

    int winID;
    unsigned int width, height;
#ifdef HAS_ANTTWEAKBAR
    TwBar *twBar;
#endif
    unsigned int winSetW, winSetH;

    Data *dat;
    Renderer *rnd;

    bool drawBBox;
    std::string camMem;


    glm::vec3 lightDir;
    bool isCamLight;
    glm::mat4 projMat;
    float apAngRad;

    ViewInteractor cam;



#ifdef HAS_LIBPNG
    std::string screenShotPath;
    bool screenShotAlpha;
    bool makeDepthScreenShot;
    bool planScreenShot;
#endif
    std::string stateFilePath;
    std::string saveFilePath;
};

}
