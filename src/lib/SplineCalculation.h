#pragma once

#define SPLINE_WITH_OPENGL

#include <GL/glew.h>
#include <glm/detail/type_vec.hpp>
#ifdef SPLINE_WITH_MMPLD
#include <Data.h>
#endif
#ifdef __linux
#include <eigen3/Eigen/Dense>
#else
#include <Eigen/Dense>
#endif
#ifdef SPLINE_WITH_BENCHMARK
#include <chrono>
#endif

namespace Spline {
    class SplineCalculator {
	public:
		unsigned int m;     // Knot vector size - 1 (h + p)
    private:
        unsigned int n;     // Frame count
        unsigned int h;     // Count of control points to calculate
        unsigned int p;     // Degree of spline curve
        unsigned int s;     // Particle count

        Eigen::MatrixXf AT;
        Eigen::MatrixXf Inv;

        GLuint id, pid;

        void setUpFitting();
#ifdef SPLINE_WITH_OPENGL
        const char*getShaderSource();
#endif
    public:
#ifdef SPLINE_WITH_BENCHMARK
        unsigned int calctime;
#endif

        float* knotVector;

        static float calculateBasisFunction(int i, int p, float t, float *knots);

        /**
         * Reads a MMPLD particle file into a continuous block of vectors, discarding
         * color and radius data. The data is stored particle major, so that there are s blocks
         * each containing all positions of the specific particle.
         */
#ifdef SPLINE_WITH_MMPLD
        glm::vec3* prepareMMPLD(const char* filename, unsigned int startpos);
#endif
        /**
         * Calculates h control points for particle data stored in data. Data is expected to contain
         * (n * s) position vectors, particle major (so that there are s blocks containing
         * all positions of the specific particle)
         */
#ifdef SPLINE_WITH_OPENGL
        glm::vec3*fitSplineShader(glm::vec3 *data);
#endif

        /**
         * Calculates h control points for particle data stored in data. Data is expected to contain
         * (n * s) position vectors, particle major (so that there are s blocks containing
         * all positions of the specific particle)
         */
        glm::vec3*fitSplineCPU(glm::vec3 *data);

        /**
         * Sets up a new SplineCalculator with the given parameters. A new compute shader is compiled and linked
         * in the process (if SPLINE_WITH_OPENGL is defined), so a runtime exception might be thrown if that fails.
         */
        SplineCalculator(unsigned int n, unsigned int h, unsigned int p, unsigned int s);

        /**
         * Frees the calculator and reserved OpenGL resources
         */
        ~SplineCalculator(void);
    };
}