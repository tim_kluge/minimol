#include "StateFile.h"
#include <algorithm>
#include <stdexcept>


minimol::StateFile::StateFile(void) : values() {
    // intentionally empty
}


minimol::StateFile::~StateFile(void) {
    // intentionally empty
}


std::vector<std::string> minimol::StateFile::keys(void) const {
	std::vector<std::string> rv;
	typedef std::unordered_map<std::string, std::string> my_map;
	my_map::const_iterator end = this->values.cend();
	for (my_map::const_iterator i = this->values.cbegin(); i != end; i++) {
		rv.push_back(i->first);
	}
	return rv;
}


template <class T>
static void replace_all(T& str, typename T::value_type ch, const T& txt) {
    size_t pos = 0;
    while (pos != T::npos) {
        pos = str.find(ch, pos);
        if (pos != T::npos) {
            str.replace(pos, 1, txt);
            pos += txt.length();
        }
    }
}


template <class T>
static void replace_all(T& str, const T& src, const T& txt) {
    size_t pos = 0;
    while (pos != T::npos) {
        pos = str.find(src, pos);
        if (pos != T::npos) {
            str.replace(pos, src.length(), txt);
            pos += txt.length();
        }
    }
}


void minimol::StateFile::save(std::string path) {
    FILE *f = fopen(path.c_str(), "wt");
    fprintf(f, "# State file\n");

    std::vector<std::string> keys;
    keys.reserve(this->values.size());
    for(auto e : this->values) {
        keys.push_back(e.first);
    }
    std::sort(keys.begin(), keys.end());

    for(auto key_idx : keys) {
        std::string key = key_idx;
        replace_all<std::string>(key, '`', "``");
        replace_all<std::string>(key, '\n', "`n");
        replace_all<std::string>(key, '\r', "`t");
        replace_all<std::string>(key, '=', "`=");
        std::string val = this->values[key_idx];
        replace_all<std::string>(val, '`', "``");
        replace_all<std::string>(val, '\n', "`n");
        replace_all<std::string>(val, '\r', "`t");

        fprintf(f, "%s=%s\n", key.c_str(), val.c_str());
    }
    fclose(f);

}


void minimol::StateFile::load(std::string path) {
    FILE *f = fopen(path.c_str(), "rt");
    if (f == nullptr) throw std::runtime_error("Cannot open file");

    this->clear();

    const size_t max_line_len = 32 * 1024;
    char line_buf[max_line_len + 1];
    line_buf[max_line_len] = 0;

    while (!feof(f)) {
        if (fgets(line_buf, max_line_len, f) == nullptr) {
            if (feof(f)) break;
            throw std::runtime_error("Failed to read text line");
        }
        if (line_buf[0] == 0) continue;
        if (line_buf[0] == '#') continue;
        if (line_buf[0] == '\r') continue;
        if (line_buf[0] == '\n') continue;

        std::string line(line_buf);
        while (line[line.length() - 1] == '\n') line.erase(line.length() - 1);
        while (line[line.length() - 1] == '\r') line.erase(line.length() - 1);

        size_t split = line.find('=');
        while ((split != std::string::npos) && (split == 0) && (line[split - 1] == '`')) split = line.find('=', split + 1);
        if (split == std::string::npos) continue;

        std::string key = line.substr(0, split);
        replace_all<std::string>(key, "``", "`");
        replace_all<std::string>(key, "`n", "\n");
        replace_all<std::string>(key, "`r", "\r");
        replace_all<std::string>(key, "`=", "=");
        std::string val = line.substr(split + 1);
        replace_all<std::string>(val, "``", "`");
        replace_all<std::string>(val, "`n", "\n");
        replace_all<std::string>(val, "`r", "\r");

        this->values[key] = val;
    }

    fclose(f);
}
