#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <GL/freeglut.h>
#include "SplineCalculation.h"
#include <chrono>

using namespace std::chrono;

void benchmark(const unsigned int n, const unsigned int h, const unsigned int p, const unsigned int s){
    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    glm::vec3* points = new glm::vec3[n * s];
 #pragma omp parallel
    for(int i = 0; i < s; i++){
        for(int t = 0; t < n; t++){
            points[i * n + t].x = float(t) / n;
            points[i * n + t].y = -(float(t) / n);
            points[i * n + t].z = float(t) / n;
        }
    }
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    Spline::SplineCalculator* calculator = new Spline::SplineCalculator(n, h, p, s);
    high_resolution_clock::time_point t3 = high_resolution_clock::now();
    glm::vec3* r = calculator->fitSplineCPU(points);
    high_resolution_clock::time_point t4 = high_resolution_clock::now();
    delete[] r;
    high_resolution_clock::time_point t5 = high_resolution_clock::now();
    r = calculator->fitSplineShader(points);
    high_resolution_clock::time_point t6 = high_resolution_clock::now();
    delete[] r;
    std::cout << n << " " << h << " " << p << " " << s << " "
        << duration_cast<microseconds>( t2 - t1 ).count() << " "	// Data Setup time
        << duration_cast<microseconds>( t4 - t3 ).count() << " "	// CPU time
        << calculator->calctime << " "								// OpenGL shader execution time
        << duration_cast<microseconds>( t6 - t5 ).count() << " "	// OpenGL shader time
        << duration_cast<microseconds>( t3 - t2 ).count() + duration_cast<microseconds>( t6 - t5 ).count() << " " << std::endl; // Total OpenGL time
    delete[] points;
    delete calculator;
}

void drawfunc(void){
	for (unsigned int s = 30000; s <= 30000; s *= 2){
        benchmark(50, 20, 5, s);
    }

    std::cout << "End" << std::endl;
	exit(0);
    ::glutHideWindow();
    ::glutDestroyWindow(::glutGetWindow());
}

int main(int argc, char* argv[]) {
    std::cout << std::endl
    << "Benchmark" << std::endl
    << std::endl;

    ::glutInit(&argc, argv);
    ::glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    ::glutInitWindowSize(1280, 720);
    ::glutInitContextVersion(4, 5);
    ::glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
    ::glutCreateWindow("Benchmark");
    GLenum err = ::glewInit();
    if(err != GLEW_OK) {
        std::cerr << "glewInit failed" << std::endl;
        exit(3);
    }
    ::glutDisplayFunc(drawfunc);
    ::glutShowWindow();
    ::glutMainLoop();
}