# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.2

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/tim/uni/minimol/benchmark

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/tim/uni/minimol/benchmark

# Include any dependencies generated for this target.
include CMakeFiles/splinebench.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/splinebench.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/splinebench.dir/flags.make

CMakeFiles/splinebench.dir/splinebench.cpp.o: CMakeFiles/splinebench.dir/flags.make
CMakeFiles/splinebench.dir/splinebench.cpp.o: splinebench.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/tim/uni/minimol/benchmark/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/splinebench.dir/splinebench.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/splinebench.dir/splinebench.cpp.o -c /home/tim/uni/minimol/benchmark/splinebench.cpp

CMakeFiles/splinebench.dir/splinebench.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/splinebench.dir/splinebench.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/tim/uni/minimol/benchmark/splinebench.cpp > CMakeFiles/splinebench.dir/splinebench.cpp.i

CMakeFiles/splinebench.dir/splinebench.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/splinebench.dir/splinebench.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/tim/uni/minimol/benchmark/splinebench.cpp -o CMakeFiles/splinebench.dir/splinebench.cpp.s

CMakeFiles/splinebench.dir/splinebench.cpp.o.requires:
.PHONY : CMakeFiles/splinebench.dir/splinebench.cpp.o.requires

CMakeFiles/splinebench.dir/splinebench.cpp.o.provides: CMakeFiles/splinebench.dir/splinebench.cpp.o.requires
	$(MAKE) -f CMakeFiles/splinebench.dir/build.make CMakeFiles/splinebench.dir/splinebench.cpp.o.provides.build
.PHONY : CMakeFiles/splinebench.dir/splinebench.cpp.o.provides

CMakeFiles/splinebench.dir/splinebench.cpp.o.provides.build: CMakeFiles/splinebench.dir/splinebench.cpp.o

CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.o: CMakeFiles/splinebench.dir/flags.make
CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.o: /home/tim/uni/minimol/src/lib/SplineCalculation.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/tim/uni/minimol/benchmark/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.o -c /home/tim/uni/minimol/src/lib/SplineCalculation.cpp

CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/tim/uni/minimol/src/lib/SplineCalculation.cpp > CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.i

CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/tim/uni/minimol/src/lib/SplineCalculation.cpp -o CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.s

CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.o.requires:
.PHONY : CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.o.requires

CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.o.provides: CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.o.requires
	$(MAKE) -f CMakeFiles/splinebench.dir/build.make CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.o.provides.build
.PHONY : CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.o.provides

CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.o.provides.build: CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.o

# Object files for target splinebench
splinebench_OBJECTS = \
"CMakeFiles/splinebench.dir/splinebench.cpp.o" \
"CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.o"

# External object files for target splinebench
splinebench_EXTERNAL_OBJECTS =

splinebench: CMakeFiles/splinebench.dir/splinebench.cpp.o
splinebench: CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.o
splinebench: CMakeFiles/splinebench.dir/build.make
splinebench: /usr/lib/x86_64-linux-gnu/libglut.so
splinebench: /usr/lib/x86_64-linux-gnu/libXmu.so
splinebench: /usr/lib/x86_64-linux-gnu/libXi.so
splinebench: /usr/lib/x86_64-linux-gnu/libGLU.so
splinebench: /usr/lib/x86_64-linux-gnu/libGL.so
splinebench: /usr/lib/x86_64-linux-gnu/libGLEW.so
splinebench: CMakeFiles/splinebench.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable splinebench"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/splinebench.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/splinebench.dir/build: splinebench
.PHONY : CMakeFiles/splinebench.dir/build

CMakeFiles/splinebench.dir/requires: CMakeFiles/splinebench.dir/splinebench.cpp.o.requires
CMakeFiles/splinebench.dir/requires: CMakeFiles/splinebench.dir/home/tim/uni/minimol/src/lib/SplineCalculation.cpp.o.requires
.PHONY : CMakeFiles/splinebench.dir/requires

CMakeFiles/splinebench.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/splinebench.dir/cmake_clean.cmake
.PHONY : CMakeFiles/splinebench.dir/clean

CMakeFiles/splinebench.dir/depend:
	cd /home/tim/uni/minimol/benchmark && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/tim/uni/minimol/benchmark /home/tim/uni/minimol/benchmark /home/tim/uni/minimol/benchmark /home/tim/uni/minimol/benchmark /home/tim/uni/minimol/benchmark/CMakeFiles/splinebench.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/splinebench.dir/depend

