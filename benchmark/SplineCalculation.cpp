#include "SplineCalculation.h"
#include <iostream>
#include <sstream>
#include <glm/glm.hpp>
#include <string.h>
#include <fstream>
#include <streambuf>
#ifdef __linux
#include <eigen3/Eigen/Dense>
#else
#include <Eigen/Dense>
#endif
#include <stdio.h>
#ifdef WIN32
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif

float Spline::SplineCalculator::calculateBasisFunction(int i, int p, float t, float *knots) {
	if (p == 0) {
		if (knots[i] <= t && t < knots[i + 1]) {
			return 1.0;
		}
		else {
			return 0.0;
		}
	}

	float faktorANumerator = t - knots[i];
	float faktorADenominator = (knots[i + p] - knots[i]);
	float faktorA = 0.0;
	if (faktorADenominator != 0) faktorA = faktorANumerator / faktorADenominator;

	float faktorBNumerator = knots[i + p + 1] - t;
	float faktorBDenominator = knots[i + p + 1] - knots[i + 1];
	float faktorB = 0.0;
	if (faktorBDenominator != 0) faktorB = faktorBNumerator / faktorBDenominator;

	return faktorA * calculateBasisFunction(i, p - 1, t, knots) +
		faktorB * calculateBasisFunction(i + 1, p - 1, t, knots);
}

Spline::SplineCalculator::~SplineCalculator(void){
#ifdef SPLINE_WITH_OPENGL
    glDeleteShader(this->id);
    glDeleteProgram(this->pid);
#endif
    delete[] this->knotVector;
}

Spline::SplineCalculator::SplineCalculator(const unsigned int n, const unsigned int h, const unsigned int p, const unsigned int s)
    : n(n), h(h), p(p), s(s), m(h + p){

#ifdef SPLINE_WITH_OPENGL
	this->pid = ::glCreateProgram();
    this->id = ::glCreateShader(GL_COMPUTE_SHADER);

    const char* src = this->getShaderSource();
    ::glShaderSource(this->id, 1, &src, nullptr);
    ::glCompileShader(this->id);
    GLint comp_stat = GL_FALSE;
    ::glGetShaderiv(this->id, GL_COMPILE_STATUS, &comp_stat);
    if (comp_stat == GL_FALSE) {
        GLint maxLength = 0;
        ::glGetShaderiv(this->id, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        char *errorLog = new char[maxLength];
        ::glGetShaderInfoLog(this->id, maxLength, &maxLength, &errorLog[0]);
		std::cout << errorLog << std::endl;
        throw std::runtime_error(std::string(&errorLog[0]));
    }

    delete[] src;

    ::glAttachShader(this->pid, this->id);
    ::glLinkProgram(this->pid);

    GLint isLinked = 0;
    ::glGetProgramiv(this->pid, GL_LINK_STATUS, &isLinked);
    if (isLinked == GL_FALSE) {
        GLint maxLength = 0;
        ::glGetProgramiv(this->pid, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        char* errorLog = new char[maxLength];
        ::glGetProgramInfoLog(this->pid, maxLength, &maxLength, &errorLog[0]);
		delete[] errorLog;

        throw std::runtime_error(errorLog);
    }
#endif

    // Knotenvektor
    unsigned int m = h + p;
    this->knotVector = new float[m + 1];

    for (int i = 0; i <= p; i++) knotVector[i] = 0;
    for (int i = 1; i <= h - 1 - p; i++) knotVector[i + p] = float(i) / float(h - p);
    for (int i = m - p; i <= m; i++) knotVector[i] = 1.0F;

	setUpFitting();
}

#ifdef SPLINE_WITH_MMPLD
glm::vec3* Spline::SplineCalculator::prepareMMPLD(const char* filename, unsigned int startpos){
    minimol::Data data;
	float tempData[3];

    glm::vec3* particleData = new glm::vec3[this->n * this->s];

    for (unsigned int y = 0; y < this->n; ++y) {
        data.loadMMPLD(filename, y + startpos);
        for (size_t lidx = 0; lidx < data.size(); lidx++) {
            const minimol::Data::List &list = data[static_cast<int>(lidx)];

            uint64_t countParticles = list.GetCount();
            switch (list.GetVertexDataType()) {
                case minimol::Data::VERTDATA_NONE:
                    continue;
                case minimol::Data::VERTDATA_FLOAT_XYZ:
                case minimol::Data::VERTDATA_FLOAT_XYZR:
                    for (unsigned int x = 0; x < countParticles; ++x) {
						memcpy(&tempData, ((char*) list.GetVertexData()) + list.GetVertexDataStride() * x, sizeof(float) * 3);
                        unsigned int ind = (x * this->n + y);

                        particleData[ind].x = tempData[0];
                        particleData[ind].y = tempData[1];
                        particleData[ind].z = tempData[2];
                    }
                    break;
                default:
                    continue;
            }
        }
    }

    return particleData;
}
#endif

void Spline::SplineCalculator::setUpFitting(){
    Eigen::MatrixXf AE(n-1, h-1);

    const unsigned int Arows = n - 1;
    const unsigned int Acols = h - 1;

#pragma omp parallel
    for (int r = 0; r < Arows; r++){
        float t = (float(r) + 1.0f) / (float(n) - 1.0f);

        for (int c = 0; c < Acols; c++) AE(r, c) = calculateBasisFunction(c + 1, p - 1, t, this->knotVector);
    }
    this->AT = AE.transpose();
    Eigen::MatrixXf ATA = AT * AE;
	this->Inv = ATA.inverse();
}

glm::vec3* Spline::SplineCalculator::fitSplineCPU(glm::vec3 *data){
    glm::vec3* res = new glm::vec3[this->s * this->h];

 #pragma omp parallel
    for(int p = 0; p < this->s; p++){
        res[p*h] = data[p * n];

        glm::vec3* pointsMiddle = new glm::vec3[n-1];
        for(int r = 1; r < n; r++){
            pointsMiddle[r-1] = data[p * n + r];
        }

        glm::vec3* atransmiddle = new glm::vec3[h-1];
        for(int r2 = 0; r2 < h - 1; r2++){
            glm::vec3 sum(0,0,0);
            for(int c2 = 0; c2 < n - 1; c2++){
                sum += AT(r2, c2) * pointsMiddle[c2+1];
            }
            atransmiddle[r2] = sum;
        }

        glm::vec3* x = new glm::vec3[h-1];
		for (int r2 = 0; r2 < h - 1; r2++){
			glm::vec3 sum(0,0,0);
			for (int c2 = 0; c2 < h - 1; c2++){
				sum += Inv(r2, c2) * atransmiddle[c2];
			}
			x[r2] = sum;
			res[p * h + (r2 + 1)] = x[r2];
		}
		res[p*h + (h - 1)] = data[p * n + (n - 1)];

		delete[] x, atransmiddle, pointsMiddle;
    }

    return res;
}

#ifdef SPLINE_WITH_OPENGL
glm::vec3* Spline::SplineCalculator::fitSplineShader(glm::vec3 *data){
    ::glUseProgram(this->pid);

    GLuint inputssbo = 0;
    ::glGenBuffers(1, &inputssbo);
    ::glBindBuffer(GL_SHADER_STORAGE_BUFFER, inputssbo);
    ::glBufferData(GL_SHADER_STORAGE_BUFFER, this->n * this->s * 3 * sizeof(float), data, GL_DYNAMIC_DRAW);
    ::glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, inputssbo);

    GLuint knotssbo = 0;
    ::glGenBuffers(1, &knotssbo);
    ::glBindBuffer(GL_SHADER_STORAGE_BUFFER, knotssbo);
    ::glBufferData(GL_SHADER_STORAGE_BUFFER, (m + 1) * sizeof(float), this->knotVector, GL_DYNAMIC_DRAW);
    ::glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, knotssbo);

    GLuint invssbo = 0;
    ::glGenBuffers(1, &invssbo);
    ::glBindBuffer(GL_SHADER_STORAGE_BUFFER, invssbo);
    ::glBufferData(GL_SHADER_STORAGE_BUFFER, (h-1)*(h-1)*sizeof(float), Inv.data(), GL_DYNAMIC_DRAW);
    ::glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 5, invssbo);

    GLuint assbo = 0;
    ::glGenBuffers(1, &assbo);
    ::glBindBuffer(GL_SHADER_STORAGE_BUFFER, assbo);
    ::glBufferData(GL_SHADER_STORAGE_BUFFER, (n-1)*(h-1)*sizeof(float), AT.data(), GL_DYNAMIC_DRAW);
    ::glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 7, assbo);

    GLuint outputssbo = 0;
    glm::vec3* cp = new glm::vec3[h*s];

    ::glGenBuffers(1, &outputssbo);
    ::glBindBuffer(GL_SHADER_STORAGE_BUFFER, outputssbo);
    ::glBufferData(GL_SHADER_STORAGE_BUFFER, h * 3 * s * sizeof(float), (void*) cp, GL_DYNAMIC_DRAW);
    ::glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, outputssbo);

#ifdef SPLINE_WITH_BENCHMARK
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
#endif

	unsigned int z = static_cast<unsigned int>(ceilf(float(s) / float(100)));
	/* GLint d;
	::glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &d); */

	// std::cout << x << ", " << z << std::endl;
    ::glDispatchCompute(z, 3, 1);
    ::glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

    ::glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
    ::glBindBuffer(GL_SHADER_STORAGE_BUFFER, outputssbo);

    glm::vec3* output = (glm::vec3*) glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);
#ifdef SPLINE_WITH_BENCHMARK
    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    this->calctime = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
#endif
    memcpy(cp, output, sizeof(glm::vec3) * s * h);

    ::glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    ::glUseProgram(0);

    return cp;
};

const char* Spline::SplineCalculator::getShaderSource(){
    std::ostringstream ss;
	char dir[256];

	unsigned int x = static_cast<unsigned int>(std::fmin(s, 100));

	ss << "#version 430\r\n"
		"\r\n"
		"const uint p = " << p << ";\r\n"
		"const uint n = " << n << ";\r\n"
		"const uint h = " << h << ";\r\n"
		"const uint s = " << s << ";\r\n"
		"const uint m = n + p + 1;\r\n"
		"layout(local_size_x = " << x << ", \r\n"
		"local_size_y = 3,\r\n"
		"local_size_z = 1) in;\r\n\r\n";

	std::string path;

	char result[FILENAME_MAX];
	GetCurrentDir(result, sizeof(result));
	bool passednull = false;
	for (int i = FILENAME_MAX - 1; i >= 0; i--){
		if (result[i] == '\0') passednull = true;
		if (result[i] == '\\' && passednull){
			result[i] = '\0';
			break;
		}
	}
	path = std::string(result) + "\\src\\spline.glslcs";

	std::ifstream cs(path);
	ss << cs.rdbuf();

    char* tmp = new char[ss.str().length()+1];
    std::strcpy (tmp, ss.str().c_str());

    return tmp;
}
#endif